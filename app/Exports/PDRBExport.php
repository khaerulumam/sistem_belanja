<?php

namespace App\Exports;

use App\Models\LapanganUsaha;
use App\Models\ProdukDomestikRegionalBruto;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithCharts;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\Border;

class PDRBExport implements FromView, WithStyles, WithCharts
{
    protected $sektor;
    function __construct($sektor)
    {
        $this->sektor = $sektor;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function view(): View
    {
        $data = ProdukDomestikRegionalBruto::where('lapangan_usaha_id', $this->sektor)->orderBy('tahun')->get();
        return view('export.table', compact('data'));
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('1')->getFont()->setBold(true);

        $column = $sheet->getHighestColumn();
        $row = $sheet->getHighestRow();

        $sheet->getStyle('A1:' . $column . $row)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_MEDIUM);
    }

    public function charts()
    {
        $sektor = LapanganUsaha::findOrFail($this->sektor);
        $nama = $sektor->nama;
        $jumlah = ProdukDomestikRegionalBruto::where('lapangan_usaha_id', $this->sektor)->count();
        $helper = new ChartHelper();
        $chart = $helper->get_chart("Produk Domestik Regional Bruto ($nama)", $jumlah);

        return $chart;
    }
}
