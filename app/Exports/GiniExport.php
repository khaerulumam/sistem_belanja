<?php

namespace App\Exports;

use App\Models\IndeksGini;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithCharts;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\Border;

class GiniExport implements FromView, WithStyles, WithCharts
{

    /**
     * @return \Illuminate\Support\Collection
     */
    public function view(): View
    {
        $data = IndeksGini::orderBy('tahun')->get();
        return view('export.table', compact('data'));
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('1')->getFont()->setBold(true);

        $column = $sheet->getHighestColumn();
        $row = $sheet->getHighestRow();

        $sheet->getStyle('A1:' . $column . $row)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_MEDIUM);
    }

    public function charts()
    {
        $jumlah = IndeksGini::count();
        $helper = new ChartHelper();
        $chart = $helper->get_chart('Indeks Gini', $jumlah);

        return $chart;
    }
}
