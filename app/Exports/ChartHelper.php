<?php

namespace App\Exports;

use PhpOffice\PhpSpreadsheet\Chart\Chart;
use PhpOffice\PhpSpreadsheet\Chart\DataSeries;
use PhpOffice\PhpSpreadsheet\Chart\DataSeriesValues;
use PhpOffice\PhpSpreadsheet\Chart\Legend;
use PhpOffice\PhpSpreadsheet\Chart\PlotArea;
use PhpOffice\PhpSpreadsheet\Chart\Title;

class ChartHelper
{
    public function get_chart($title, $jumlah)
    {
        $last =  $jumlah + 1;
        $label = [
            new DataSeriesValues('String', 'Worksheet!$C$1', null, 1)
        ];
        $axis = [
            new DataSeriesValues('Number', 'Worksheet!$B$2:$B$' . $last, null, $jumlah)
        ];
        $values = [
            new DataSeriesValues('Number', 'Worksheet!$C$2:$C$' . $last, null, $jumlah)
        ];

        $series = new DataSeries(
            DataSeries::TYPE_LINECHART,
            DataSeries::GROUPING_STACKED,
            range(0, count($values) - 1),
            $label,
            $axis,
            $values
        );
        $plot   = new PlotArea(null, [$series]);

        $legend = new Legend(Legend::POSITION_TOP, null, false);
        $chart  = new Chart('Chart', new Title($title), $legend, $plot);

        $chart->setTopLeftPosition('E2');
        $chart->setBottomRightPosition('T24');

        return $chart;
    }

    public function get_chart_proyeksi($title, $jumlah)
    {
        $last =  $jumlah + 1;
        $label = [
            new DataSeriesValues('String', 'Worksheet!$C$1', null, 1),
            new DataSeriesValues('String', 'Worksheet!$D$1', null, 1),
            new DataSeriesValues('String', 'Worksheet!$E$1', null, 1),
            new DataSeriesValues('String', 'Worksheet!$F$1', null, 1)
        ];
        $axis = [
            new DataSeriesValues('Number', 'Worksheet!$B$2:$B$' . $last, null, $jumlah)
        ];
        $values = [
            new DataSeriesValues('Number', 'Worksheet!$C$2:$C$' . $last, null, $jumlah),
            new DataSeriesValues('Number', 'Worksheet!$D$2:$D$' . $last, null, $jumlah),
            new DataSeriesValues('Number', 'Worksheet!$E$2:$E$' . $last, null, $jumlah),
            new DataSeriesValues('Number', 'Worksheet!$F$2:$F$' . $last, null, $jumlah)
        ];

        $series = new DataSeries(
            DataSeries::TYPE_LINECHART,
            null,
            range(0, count($values) - 1),
            $label,
            $axis,
            $values
        );

        $plot   = new PlotArea(null, [$series]);
        $legend = new Legend(Legend::POSITION_TOP, null, false);

        $chart  = new Chart('Chart', new Title($title), $legend, $plot);

        $chart->setTopLeftPosition('H2');
        $chart->setBottomRightPosition('T24');

        return $chart;
    }
}
