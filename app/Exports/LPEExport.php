<?php

namespace App\Exports;

use App\Models\LajuPertumbuhanEkonomi;
use App\Models\LapanganUsaha;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCharts;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\Border;

class LPEExport implements FromView, WithStyles, WithCharts, ShouldAutoSize
{
    protected $sektor;
    function __construct($sektor)
    {
        $this->sektor = $sektor;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function view(): View
    {
        $data = LajuPertumbuhanEkonomi::where('lapangan_usaha_id', $this->sektor)->orderBy('tahun')->get();
        return view('export.table-proyeksi', compact('data'));
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('1')->getFont()->setBold(true);

        $column = $sheet->getHighestColumn();
        $row = $sheet->getHighestRow();

        $sheet->getStyle('A1:' . $column . $row)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_MEDIUM);
    }

    public function charts()
    {
        $sektor = LapanganUsaha::findOrFail($this->sektor);
        $nama = $sektor->nama;
        $jumlah = LajuPertumbuhanEkonomi::where('lapangan_usaha_id', $this->sektor)->count();
        $helper = new ChartHelper();
        $chart = $helper->get_chart_proyeksi("Laju Pertumbuhan Ekonomi ($nama)", $jumlah);

        return $chart;
    }
}
