<?php

namespace App\Exports;

use App\Models\TingkatPengangguran;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithCharts;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\Border;

class PengangguranExport implements FromView, WithStyles, WithCharts
{

    /**
     * @return \Illuminate\Support\Collection
     */
    public function view(): View
    {
        $data = TingkatPengangguran::orderBy('tahun')->get();
        return view('export.table', compact('data'));
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('1')->getFont()->setBold(true);

        $column = $sheet->getHighestColumn();
        $row = $sheet->getHighestRow();

        $sheet->getStyle('A1:' . $column . $row)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_MEDIUM);
    }

    public function charts()
    {
        $jumlah = TingkatPengangguran::count();
        $helper = new ChartHelper();
        $chart = $helper->get_chart('Tingkat Pengangguran Terbuka', $jumlah);

        return $chart;
    }
}
