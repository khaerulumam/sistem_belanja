<?php

namespace App\Exports;

use App\Models\Inflasi;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCharts;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\Border;

class InflasiExport implements FromView, WithStyles, ShouldAutoSize, WithCharts
{

    /**
     * @return \Illuminate\Support\Collection
     */
    public function view(): View
    {
        $data = Inflasi::orderBy('tahun')->get();
        return view('export.table-proyeksi', compact('data'));
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('1')->getFont()->setBold(true);

        $column = $sheet->getHighestColumn();
        $row = $sheet->getHighestRow();

        $sheet->getStyle('A1:' . $column . $row)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_MEDIUM);
    }

    public function charts()
    {
        $jumlah = Inflasi::count();
        $helper = new ChartHelper();
        $chart = $helper->get_chart_proyeksi('Inflasi', $jumlah);

        return $chart;
    }
}
