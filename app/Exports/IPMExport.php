<?php

namespace App\Exports;

use App\Models\IndeksPembangunanManusia;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithCharts;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\Border;

class IPMExport implements FromView, WithStyles, WithCharts
{

    /**
     * @return \Illuminate\Support\Collection
     */
    public function view(): View
    {
        $data = IndeksPembangunanManusia::orderBy('tahun')->get();
        return view('export.table', compact('data'));
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('1')->getFont()->setBold(true);

        $column = $sheet->getHighestColumn();
        $row = $sheet->getHighestRow();

        $sheet->getStyle('A1:' . $column . $row)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_MEDIUM);
    }

    public function charts()
    {
        $jumlah = IndeksPembangunanManusia::count();
        $helper = new ChartHelper();
        $chart = $helper->get_chart('Indeks Pembangunan Manusia', $jumlah);

        return $chart;
    }
}
