<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property int $id_barang
 * @property integer $id_user_pembeli
 * @property int $jumlah_barang
 * @property float $jumlah_harga
 * @property string $periode
 * @property string $created_at
 * @property string $updated_at
 * @property Barang $barang
 * @property User $user
 */
class Belanja extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'belanja';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id_barang', 'id_user_pembeli', 'jumlah_barang', 'jumlah_harga', 'periode', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function barang()
    {
        return $this->belongsTo('App\Models\Barang', 'id_barang');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id_user_pembeli');
    }
}
