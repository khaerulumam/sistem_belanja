<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_jenis_barang
 * @property string $merk
 * @property string $created_at
 * @property string $updated_at
 * @property JenisBarang $jenisBarang
 * @property Belanja[] $belanjas
 */
class Barang extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'barang';

    /**
     * @var array
     */
    protected $fillable = ['id_jenis_barang', 'merk', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jenisBarang()
    {
        return $this->belongsTo('App\Models\JenisBarang', 'id_jenis_barang');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function belanjas()
    {
        return $this->hasMany('App\Models\Belanja', 'id_barang');
    }
}
