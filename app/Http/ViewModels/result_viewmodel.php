<?php

namespace App\Http\ViewModels;

class ResultViewModel
{
    public $status;
    public $message;
    public $redirectUrl;
}
