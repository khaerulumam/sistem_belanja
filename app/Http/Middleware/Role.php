<?php

namespace App\Http\Middleware;

use App\Helpers\Functions;
use Closure;
use Auth;
use App\User;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param   string $role_definition
     * @return mixed
     */
    public function handle($request, Closure $next, $role_definition)
    {
        if (Auth::guest()) {
            return redirect(route('login', [], false));
        }

        if ($role_definition) {
            $permitted_roles = is_array($role_definition) ? $role_definition : explode('|', $role_definition);

            if (!in_array(Functions::getRoleName(Auth::user()->jenis_pengguna_id), $permitted_roles)) {
                abort(403, 'Anda tidak memiliki akses ke halaman ini.');
            }
        }

        return $next($request);
    }
}
