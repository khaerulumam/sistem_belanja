<?php

namespace App\Http\Controllers;

use App\Helpers\Variables;
use Exception;
use Session;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function toJsonError(\Throwable $e)
    {
        $hasil = [
            Variables::STATUS => 0,
            Variables::MESSAGE => $e->getMessage()
        ];
        return json_encode($hasil);
    }

    public function toJsonSuccess($url = "", $message = "Data berhasil disimpan")
    {
        $this->setSuccessNotification($message);
        $hasil = [
            Variables::STATUS => 1,
            Variables::REDIRECT_URL => $url
        ];

        return json_encode($hasil);
    }

    public function setSuccessNotification($message = 'Data berhasil disimpan')
    {
        Session::put(Variables::STATUS, 1);
        Session::put(Variables::MESSAGE, $message);
    }

    public function setSuccessNotificationDelete()
    {
        $this->setSuccessNotification("Data berhasil dihapus");
    }

    public function setErrorNotification($message)
    {
        Session::put(Variables::STATUS, 2);
        Session::put(Variables::MESSAGE, $message);
    }

    public function setErrorNotificationDelete()
    {
        $this->setErrorNotification("Data gagal dihapus");
    }

    public function setExceptionMessage(\Throwable $e)
    {
        $this->setErrorNotification($e->getMessage());
    }

    public function toErrorModal(\Throwable $e)
    {
        $pesan = $e->getMessage();
        return view('partials._error', compact('pesan'));
    }

    public function getChart($data)
    {
        $label = [];
        $aktual = [];

        foreach ($data as $item) {
            $label[] = $item->tahun;
            $aktual[] = $item->nilai;
        }

        $chart = [
            'label' => $label,
            'aktual' => $aktual,
            'pesimis' => [],
            'moderat' => [],
            'optimis' => [],
        ];
        $chart = json_encode($chart);

        return $chart;
    }

    public function getChartProyeksi($data)
    {
        $label = [];
        $aktual = [];
        $pesimis = [];
        $moderat = [];
        $optimis = [];

        foreach ($data as $item) {
            $label[] = $item->tahun;
            $aktual[] = $item->nilai_aktual;
            $pesimis[] = $item->nilai_proyeksi_pesimis;
            $moderat[] = $item->nilai_proyeksi_moderat;
            $optimis[] = $item->nilai_proyeksi_optimis;
        }

        $chart = new ChartViewModel();
        $chart->label = $label;
        $chart->aktual = $aktual;
        $chart->pesimis = $pesimis;
        $chart->moderat = $moderat;
        $chart->optimis = $optimis;

        return json_encode($chart);
    }

    public function getChartProyeksiOld($data)
    {
        $label = [];
        $aktual = [];
        $pesimis = [];
        $moderat = [];
        $optimis = [];

        foreach ($data as $item) {
            $label[] = $item->tahun;
            if ($item->nilai_aktual != null) {
                $aktual[] = $item->nilai_aktual;
            }
        }
        $jumlahAktual = count($aktual);

        foreach ($data as $item) {
            if ($item->nilai_aktual != null) {
                if ($jumlahAktual <= 1) {
                    $pesimis[] = $item->nilai_aktual;
                    $moderat[] = $item->nilai_aktual;
                    $optimis[] = $item->nilai_aktual;
                } else {
                    $pesimis[] = null;
                    $moderat[] = null;
                    $optimis[] = null;
                }
            } else {
                $pesimis[] = $item->nilai_proyeksi_pesimis;
                $moderat[] = $item->nilai_proyeksi_moderat;
                $optimis[] = $item->nilai_proyeksi_optimis;
            }
            $jumlahAktual--;
        }

        $chart = new ChartViewModel();
        $chart->label = $label;
        $chart->aktual = $aktual;
        $chart->pesimis = $pesimis;
        $chart->moderat = $moderat;
        $chart->optimis = $optimis;

        return json_encode($chart);
    }
}

class ChartViewModel
{
    public $label;
    public $aktual;
    public $pesimis;
    public $moderat;
    public $optimis;
}
