<?php

namespace App\Http\Controllers;

use App\Helpers\Variables;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $data = User::all();
        return view('user.index', compact('data'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = new User();
        return view('user.form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validateData($request);
            $this->validateDataExist($request, 0);

            $model = new User();
            $model->fill($request->all());
            $model->password = Hash::make($request->password);
            $model->jenis_pengguna_id = Variables::JENIS_PENGGUNA_ADMIN;
            $model->save();

            return $this->toJsonSuccess();
        } catch (\Throwable $th) {
            return $this->toJsonError($th);
        }
    }

    public function validateData(Request $request)
    {
        $rules = [
            'username' => 'required|string|max:100',
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
        ];

        if (empty($request->id)) {
            $rules['password'] = 'required|string|min:6';
        }

        return $this->validate($request, $rules);
    }

    public function validateDataExist(Request $request, $id)
    {
        $ada = User::where(['username' => $request->username])->first();
        if (!empty($ada)) {
            if ($id > 0 && $ada->id != $id || $id == 0) {
                throw new Exception("Username sudah digunakan.");
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::findOrFail($id);
        return view('user.form', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validateData($request);
            $this->validateDataExist($request, $id);

            $oldModel = User::findOrFail($id);
            $model = User::findOrFail($id);
            $model->fill($request->all());
            // echo "<pre>";
            // print_r($model);
            // exit();
            if (empty($request->password)) {
                $model->password = $oldModel->password;
            } else {
                $model->password = Hash::make($request->password);
            }
            $model->save();

            return $this->toJsonSuccess();
        } catch (\Exception $th) {
            return $this->toJsonError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = User::findOrFail($id);
        if ($model->delete()) {
            return $this->setSuccessNotificationDelete();
        } else {
            return $this->setErrorNotificationDelete();
        }
    }
}
