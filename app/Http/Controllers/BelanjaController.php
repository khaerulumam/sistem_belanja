<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Belanja;
use App\Models\User;
use Illuminate\Http\Request;

class BelanjaController extends Controller
{
    public function index()
    {
        $data = Belanja::all();
        return view('belanja.index', compact('data'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = new Belanja();
        $jenis = Barang::all();
        $user = User::all();
        $data->periode = date("m-Y");
        $data->jumlah_barang = 1;
        return view('belanja.form', compact('data', 'jenis', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validateData($request);

            $model = new Belanja();
            $model->fill($request->all());
            $model->periode = date("Y-m-d", strtotime("01-" . $request->periode));
            $model->jumlah_harga = str_replace(".", "", $request->jumlah_harga);
            $model->save();

            return $this->toJsonSuccess();
        } catch (\Throwable $th) {
            return $this->toJsonError($th);
        }
    }

    public function validateData(Request $request)
    {
        $rules = [
            'jumlah_barang' => 'required|integer',
            'jumlah_harga' => 'required|string|max:13',
            'periode' => 'required|date_format:m-Y',
            'id_barang' => 'required|integer',
            'id_user_pembeli' => 'nullable|integer',
        ];

        return $this->validate($request, $rules);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Belanja::findOrFail($id);
        $data->periode = date('m-Y', strtotime($data->periode));
        $data->jumlah_harga = number_format($data->jumlah_harga, 0, ',', '.');

        $jenis = Barang::all();
        $user = User::all();
        return view('belanja.form', compact('data', 'jenis', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validateData($request);

            $model = Belanja::findOrFail($id);
            $model->fill($request->all());
            $model->periode = date("Y-m-d", strtotime("01-" . $request->periode));
            $model->jumlah_harga = str_replace(".", "", $request->jumlah_harga);
            $model->save();

            return $this->toJsonSuccess();
        } catch (\Exception $th) {
            return $this->toJsonError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Belanja::findOrFail($id);
        if ($model->delete()) {
            return $this->setSuccessNotificationDelete();
        } else {
            return $this->setErrorNotificationDelete();
        }
    }
}
