<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Exception;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    protected $maxAttempts = 1;
    protected $decayMinutes = 1;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        try {
            $this->validateLogin($request);

            $user = User::where('username', $request->username)->first();
            if (empty($user)) {
                throw new Exception('User tidak ditemukan.');
            }

            // $pass = $request->password;

            // $password = Hash::make($pass);
            // if ($user->password != $password) {
            //     throw new Exception("Password Anda salah");
            // }

            if ($this->attemptLogin($request)) {
                if ($request->hasSession()) {
                    $request->session()->put('auth.password_confirmed_at', time());
                }

                return $this->toJsonSuccess(route('admin', [], false), "Anda berhasil login");
            }

            return $this->sendFailedLoginResponse($request);
        } catch (\Exception $e) {
            return $this->toJsonError($e);
        }
    }

    public function username()
    {
        return 'username';
    }
}
