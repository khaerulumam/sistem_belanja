<?php

namespace App\Http\Controllers;

use App\Models\JenisBarang;
use Illuminate\Http\Request;

class JenisBarangController extends Controller
{
    public function index()
    {
        $data = JenisBarang::all();
        return view('jenis-barang.index', compact('data'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = new JenisBarang();
        return view('jenis-barang.form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validateData($request);
            
            $model = new JenisBarang();
            $model->fill($request->all());
            $model->save();

            return $this->toJsonSuccess();
        } catch (\Throwable $th) {
            return $this->toJsonError($th);
        }
    }

    public function validateData(Request $request)
    {
        $rules = [
            'nama' => 'required|string|max:100',
        ];

        return $this->validate($request, $rules);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = JenisBarang::findOrFail($id);
        return view('jenis-barang.form', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validateData($request);
            
            $model = JenisBarang::findOrFail($id);
            $model->fill($request->all());
            $model->save();

            return $this->toJsonSuccess();
        } catch (\Exception $th) {
            return $this->toJsonError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = JenisBarang::findOrFail($id);
        if ($model->delete()) {
            return $this->setSuccessNotificationDelete();
        } else {
            return $this->setErrorNotificationDelete();
        }
    }
}
