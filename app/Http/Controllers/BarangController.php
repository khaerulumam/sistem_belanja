<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\JenisBarang;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    public function index()
    {
        $data = Barang::all();
        return view('barang.index', compact('data'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = new Barang();
        $jenis = JenisBarang::all();
        return view('barang.form', compact('data', 'jenis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validateData($request);

            $model = new Barang();
            $model->fill($request->all());
            $model->save();

            return $this->toJsonSuccess();
        } catch (\Throwable $th) {
            return $this->toJsonError($th);
        }
    }

    public function validateData(Request $request)
    {
        $rules = [
            'merk' => 'required|string|max:255',
            'id_jenis_barang' => 'required|integer',
        ];

        return $this->validate($request, $rules);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Barang::findOrFail($id);
        $jenis = JenisBarang::all();
        return view('barang.form', compact('data', 'jenis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validateData($request);

            $model = Barang::findOrFail($id);
            $model->fill($request->all());
            $model->save();

            return $this->toJsonSuccess();
        } catch (\Exception $th) {
            return $this->toJsonError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Barang::findOrFail($id);
        if ($model->delete()) {
            return $this->setSuccessNotificationDelete();
        } else {
            return $this->setErrorNotificationDelete();
        }
    }
}
