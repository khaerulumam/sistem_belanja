<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Exception;

class Functions
{
    public static function getRoleName($roleId)
    {
        switch ($roleId) {
            case Variables::JENIS_PENGGUNA_ADMIN:
                return Variables::ADMIN;
        }
        return "-";
    }

    public function GetRoute($key, $params = [], $absolute = false): string
    {
        return route($key, $params, $absolute);
    }

    public static function UploadFile(Request $request, $allowed = [], $maxSize = 5000000): string
    {
        $file = $request->file('file');
        $ext = $file->getClientOriginalExtension();
        $ext = strtolower($ext);
        $size = $file->getSize();

        if ($size == 0) {
            throw new Exception("File kosong atau tidak bisa dibaca");
        }

        if ($size > $maxSize) {
            throw new Exception("Ukuran file terlalu besar");
        }

        if (!in_array($ext, $allowed)) {
            throw new Exception("Ekstensi file tidak dikenal");
        }

        $newName = self::generateRandomString() . "_" . date('YmdHis') . "." . $ext;

        $file->move(Variables::UPLOAD_LOCATION, $newName);

        return "/" . Variables::UPLOAD_LOCATION . "/" . $newName;
    }

    public static function generateRandomString($length = 20)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function ShowFile($url)
    {
        try {
            if (empty($url)) {
                throw new Exception("Url kosong");
            }

            $split = explode(".", $url);
            $ext = end($split);
            $ext = strtolower($ext);

            if (in_array($ext, Variables::ALLOWED_EXTENSION)) {
                return "<img src='" . $url . "' class='gambar-tengah' />";
            }

            if ($ext == Variables::PDF_EXTENSION) {
                return "<object type='application/pdf' class='pdf-tengah' style='' data='" . $url . "?#toolbar=0&navpanes=0'>" .
                    "<p> Tidak dapat menampilkan preview. Klik <a href=" . $url . ">di sini</a> untuk mengunduh.</p>" .
                    "</object>";
            }

            throw new Exception("Dokumen tidak dapat ditampilkan");
        } catch (Exception $e) {
            $pesan = $e->getMessage();
            return "<span class='text-muted text-center' style='display: block;'>" . $pesan . "</span>";
        }
    }
}
