<?php

namespace App\Helpers;

class Variables
{
    const STATUS = "status";
    const MESSAGE = "message";
    const REDIRECT_URL = "url";

    const JENIS_ISI_WEB_PROFIL = 2;
    const JENIS_ISI_WEB_STRUKTUR_ORGANISASI = 3;
    const JENIS_ISI_WEB_METODE = 4;

    const ADMIN = 'admin';
    const JENIS_PENGGUNA_ADMIN = 1;

    const UPLOAD_LOCATION = 'uploads';

    const ALLOWED_EXTENSION = ['jpg', 'jpeg', 'png'];
    const PDF_EXTENSION = "pdf";
}
