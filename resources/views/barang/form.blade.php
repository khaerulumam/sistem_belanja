@php
$isEdit = $data->id > 0;
@endphp

<h3>
    <b>
        {{ $isEdit ? 'Ubah' : 'Tambah' }} Barang
    </b>
</h3>
<hr>
<form
    action="{{ $isEdit? route('editor-barang-ep', ['id' => $data->id], false): route('editor-barang-cp', [], false) }}"
    method="POST" id="main_form">
    @if ($isEdit)
        <input type="hidden" name="id" value="{{ $data->id }}">
    @endif
    <div class="form-group">
        <label class="harus">Jenis Barang</label>
        <select class="form-control chosen" name="id_jenis_barang">
            <option value="">-- Pilih --</option>
            @foreach ($jenis as $x)
                <option value="{{ $x->id }}" {{ $x->id == $data->id_jenis_barang ? "selected" : "" }}>{{ $x->nama }}</option>
            @endforeach
        </select>
        <label id="id_jenis_barang-error" class="error" for="id_jenis_barang" style="display: none">This field is required.</label>
    </div>
    <div class="form-group">
        <label class="harus">Merk</label>
        <input type="text" class="form-control" name="merk" value="{{ $data->merk }}">
    </div>
    <div class="form-group text-right">
        <button class="btn btn-primary" type="submit">Simpan</button>
    </div>
</form>
<script>
    SetValidation();
    SetChosen();
</script>
