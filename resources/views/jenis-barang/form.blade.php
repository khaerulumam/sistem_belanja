@php
$isEdit = $data->id > 0;
@endphp

<h3>
    <b>
        {{ $isEdit ? 'Ubah' : 'Tambah' }} Jenis Barang
    </b>
</h3>
<hr>
<form
    action="{{ $isEdit? route('editor-jenis-barang-ep', ['id' => $data->id], false): route('editor-jenis-barang-cp', [], false) }}"
    method="POST" id="main_form">
    @if ($isEdit)
        <input type="hidden" name="id" value="{{ $data->id }}">
    @endif
    <div class="form-group">
        <label class="harus">Nama</label>
        <input type="text" class="form-control" name="nama" value="{{ $data->nama }}">
    </div>
    <div class="form-group text-right">
        <button class="btn btn-primary" type="submit">Simpan</button>
    </div>
</form>
<script>
    SetValidation();
</script>
