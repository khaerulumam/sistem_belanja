@extends('layouts.web')

@section('title')
    Jenis Barang
@endsection

@section('content')
    <div class="card card-primary card-outline">
        <div class="card-body">
            <div class="w-100 text-right mb-3">
                <a class="btn btn-primary" onclick="OpenModal('{{ route('editor-jenis-barang-c', [], false) }}')">Tambah</a>
            </div>
            <table class="table" id="main_table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th class="nosort">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($data as $item)
                        <tr>
                            <td>{{ $no }}</td>
                            <td>{{ $item->nama }}</td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-warning"
                                        onclick="OpenModal('{{ route('editor-jenis-barang-e', [$item->id], false) }}')"><i
                                            class="fa fa-edit"></i></a>
                                    <a class="btn btn-danger"
                                        onclick="Delete('{{ route('editor-jenis-barang-d', [$item->id], false) }}')"><i
                                            class="fa fa-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                        @php
                            $no++;
                        @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    @include('partials._datatables')
@endsection
