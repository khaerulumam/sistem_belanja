@php
$isEdit = $data->id > 0;
@endphp

<h3>
    <b>
        {{ $isEdit ? 'Ubah' : 'Tambah' }} Pengguna
    </b>
</h3>
<hr>
<form
    action="{{ $isEdit ? route('editor-user_ep', ['id' => $data->id], false) : route('editor-user_cp', [], false) }}"
    method="POST" id="main_form">
    @if ($isEdit)
        <input type="hidden" name="id" value="{{ $data->id }}">
    @endif
    <div class="form-group">
        <label class="harus">Username</label>
        <input type="text" class="form-control" name="username" value="{{ $data->username }}">
    </div>
    <div class="form-group">
        <label class="{{ $isEdit ? '' : 'harus' }}">Password</label>
        <input type="password" class="form-control" name="password" value="">
    </div>
    <div class="form-group">
        <label class="harus">Nama</label>
        <input type="text" class="form-control" name="name" value="{{ $data->name }}">
    </div>
    <div class="form-group">
        <label class="harus">Email</label>
        <input type="email" class="form-control" name="email" value="{{ $data->email }}">
    </div>
    <div class="form-group text-right">
        <button class="btn btn-primary" type="submit">Simpan</button>
    </div>
</form>
<script>
    SetValidation();
</script>
