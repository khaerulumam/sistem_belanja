@extends('layouts.web')

@section('title')
    Manajemen Pengguna
@endsection

@section('content')
    <div class="card card-primary card-outline">
        <div class="card-body">
            <div class="w-100 text-right mb-3">
                <a class="btn btn-primary" onclick="OpenModal('{{ route('editor-user_c', [], false) }}')">Tambah</a>
            </div>
            <table class="table" id="main_table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Jenis Pengguna</th>
                        <th class="nosort">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($data as $item)
                        <tr>
                            <td>{{ $no }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->username }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->jenisPengguna->nama }}</td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-warning"
                                        onclick="OpenModal('{{ route('editor-user_e', [$item->id], false) }}')"><i
                                            class="fa fa-edit"></i></a>
                                    <a class="btn btn-danger"
                                        onclick="Delete('{{ route('editor-user_d', [$item->id], false) }}')"><i
                                            class="fa fa-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                        @php
                            $no++;
                        @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    @include('partials._datatables')
@endsection
