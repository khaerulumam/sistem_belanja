<link rel="stylesheet" href="/lib/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
<script src="/lib/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

<script>
    function SetDatepicker(el) {
        el = el || ".tanggal";
        $(el).datepicker({
            format: "dd-mm-yyyy"
        });
    }

    function SetPeriode(el) {
        el = el || ".periode";
        $(el).datepicker({
            format: "mm-yyyy",
            startView: "months",
            minViewMode: "months"
        });
    }
</script>
