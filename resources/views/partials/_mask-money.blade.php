<script src="/lib/mask-money/jquery.maskMoney.min.js"></script>

<script>
    function SetMaskMoney(el) {
        el = el || ".uang";
        $(el).maskMoney({
            thousands: '.',
            decimal: ',',
            allowZero: true,
            precision: 0
        });
    }
</script>
