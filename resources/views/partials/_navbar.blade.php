<ul class="navbar-nav">
    <li class="nav-item">
        <a href="/" class="nav-link">Home</a>
    </li>
    <li class="nav-item dropdown">
        <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
            class="nav-link dropdown-toggle">Profil</a>
        <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
            <li><a href="{{ route('profil', [], false) }}" class="dropdown-item">Tentang </a></li>
            <li><a href="{{ route('organisasi', [], false) }}" class="dropdown-item">Struktur Organisasi </a></li>
            <li><a href="{{ route('galeri', [], false) }}" class="dropdown-item">Galeri</a></li>
        </ul>
    </li>
    <li class="nav-item dropdown">
        <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
            class="nav-link dropdown-toggle">Statistik</a>
        <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
            <!-- Level two dropdown-->
            <li class="dropdown-submenu dropdown-hover">
                <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false" class="dropdown-item dropdown-toggle">Ekonomi</a>
                <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                    <li><a href="{{ route('pertumbuhan-ekonomi', [], false) }}" class="dropdown-item">Laju Pertumbuhan
                            Ekonomi
                            (LPE)</a></li>
                    <li><a href="{{ route('pdrb', [], false) }}" class="dropdown-item">Produk Domestik Regional Bruto
                            (PDRB)</a></li>
                    <li><a href="{{ route('inflasi', [], false) }}" class="dropdown-item">Inflasi</a></li>
                </ul>
            </li>
            <li class="dropdown-submenu dropdown-hover">
                <a id="dropdownSubMenu3 href=" #" role="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false" class="dropdown-item dropdown-toggle">Sosial</a>
                <ul aria-labelledby="dropdownSubMenu3" class="dropdown-menu border-0 shadow">
                    <li><a href="{{ route('pembangunan', [], false) }}" class="dropdown-item">Indeks Pembangunan
                            Manusia</a>
                    </li>
                    <li><a href="{{ route('kemiskinan', [], false) }}" class="dropdown-item">Tingkat Kemiskinan</a>
                    </li>
                    <li><a href="{{ route('gini', [], false) }}" class="dropdown-item">Indeks Gini</a></li>
                </ul>
            </li>
            <li class="dropdown-submenu dropdown-hover">
                <a id="dropdownSubMenu4" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false" class="dropdown-item dropdown-toggle">Ketenagakerjaan</a>
                <ul aria-labelledby="dropdownSubMenu4" class="dropdown-menu border-0 shadow">
                    <li><a href="{{ route('pengangguran', [], false) }}" class="dropdown-item">Tingkat Pengangguran
                            Terbuka
                            (TPT)</a></li>
                </ul>
            </li>
            <!-- End Level two -->
        </ul>
    </li>
    <li class="nav-item">
        <a href="{{ route('metode', [], false) }}" class="nav-link">Metode</a>
    </li>
</ul>
