@if (!empty(Session::get('status')))
    <?php
    $status = Session::get('status');
    $pesan = Session::get('message');

    Session::forget('status');
    Session::forget('message');
    ?>
    @if ($status == 1)
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-check"></i> Berhasil!</h5>
            {{ $pesan }}
        </div>
    @elseif ($status == 2)
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-ban"></i> Gagal!</h5>
            {{ $pesan }}
        </div>
    @elseif ($status == 3)
        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-ban"></i> Info!</h5>
            {{ $pesan }}
        </div>
    @endif
@endif
