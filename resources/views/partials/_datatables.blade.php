<link rel="stylesheet" href="/lib/template/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css" />
<script src="/lib/template/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/lib/template/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>

<script>
    var table = null;
    setDatatables("#main_table");

    function setDatatables(element, sortCol, direction) {
        sortCol = sortCol || 1;
        direction = direction || "asc";
        table = $(element).DataTable({
            "columnDefs": [{
                "searchable": false,
                "orderable": false,
                "targets": 0
            }, {
                "searchable": false,
                "orderable": false,
                "targets": "nosort"
            }],
            "order": [[sortCol, direction]]
        });

        table.on('order.dt search.dt', function () {
            table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        $(element).wrap("<div class='table-responsive'></div>");
    }

    var t = null;
    function SetDatatablesServerSide(el, url, filterClass, callback) {
        el = el || "#main_table";
        url = url || "";
        $(document).ready(function () {
            t = $(el).dataTable({
                "aaSorting": [1],
                "bServerSide": true,
                "bStateSave": true,
                "sAjaxSource": url,
                "bProcessing": true,
                "aoColumnDefs": [{ "bSortable": false, "aTargets": "nosort" }],
                fnServerParams: function(aoData) {
                    StopAjax();
                    if (callback != undefined) {
                        callback(aoData);
                    }
                }
                //fnServerParams: function (aoData) {
                //    //this function is called each time fnServerParams sends data to server
                //    filter.forEach(function(item, index) {
                //        aoData.push(item);
                //    });
                //    //aoData.push({ "name": "Tahun", "value": $('#Tahun').val() });
                //    //aoData.push({ "name": "UnitId", "value": $('#UnitId').val() });
                //    //aoData.push({ "name": "PegawaiId", "value": $('#PegawaiId').val() });
                //}
            });
            $(el).css("width", "100%");
            $(el).wrap("<div class='table-responsive'></div>");

            if (filterClass != undefined) {
                $(`.${filterClass}`).change(function() {
                    t.fnDraw();
                });
            }
        });
    }

    function StopAjax() {
        if (t != undefined) {
            t.fnSettings().jqXHR.abort();
        }
    }
</script>