<script src="/lib/jquery.form/jquery.form.min.js"></script>

<script>
    function SetJqueryForm(idForm) {
        idForm = idForm || "main_form";
        $("#" + idForm).ajaxForm({
            beforeSubmit: function() {
                ShowLoading();
            },
            data: {
                "_token": '{{ csrf_token() }}'
            },
            success: function(hasil) {
                var res = JSON.parse(hasil);
                if (res.status) {
                    window.onbeforeunload = function() {}
                    if (res.url !== "" && res.url !== null) {
                        location.replace(res.url);
                    } else {
                        location.reload();
                    }
                } else {
                    ShowError(res.message);
                }
            },
            error: function(a, b, c) {
                var pesan = b;
                if (c != undefined && c != "" && c != null) {
                    pesan = c;
                }
                ShowError(pesan);
            }
        });
    }

    function SetJqueryFormNoLoad(idForm, callback) {
        idForm = idForm || "main_form";
        $("#" + idForm).ajaxForm({
            beforeSubmit: function() {
                ShowLoading();
            },
            data: {
                "_token": '{{ csrf_token() }}'
            },
            success: function(hasil) {
                var res = JSON.parse(hasil);
                if (res.status) {
                    if (res.url !== "" && res.url !== null) {
                        location.replace(res.url);
                    } else {
                        location.reload();
                    }
                } else {
                    ShowError(res.message);
                }
            },
            error: function(a, b, c) {
                var pesan = b;
                if (c != undefined && c != "" && c != null) {
                    pesan = c;
                }
                ShowError(pesan);
            }
        });
    }
</script>
