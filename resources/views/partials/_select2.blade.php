<link rel="stylesheet" href="/lib/template/plugins/select2/css/select2.min.css" />
<script src="/lib/template/plugins/select2/js/select2.full.min.js"></script>
<script>
    function SetChosen(el) {
        el = el || ".chosen";
        $(el).select2({
            width: "100%"
        });
    }

    function SetChosenWithoutSearch(el) {
        $(el).select2({
            minimumResultsForSearch: -1,
            width: "100%"
        });
    }

    function SetChosenTags(el) {
        $(el).select2({
            tags: true,
            tokenSeparators: [';'],
            width: "100%"
        });
    }

    function ChosenNotSorted(el) {
        $(el).select2({
            width: "100%",
            placeholder: "-- Pilih --"
        });

        $(el).on("select2:select", function(evt) {
            var element = evt.params.data.element;
            var $element = $(element);

            $element.detach();
            $(this).append($element);
            $(this).trigger("change");
        });
    }
</script>
