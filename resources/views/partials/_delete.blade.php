<script>
    function Delete(url, message) {
        message = message || "Anda tidak akan dapat mengembalikan data ini!";
        Swal.fire({
            title: 'Apakah Anda yakin?',
            text: message,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya, hapus data ini!'
        }).then((result) => {
            if (result.value) {
                ShowLoading();
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        "_token": '{{ csrf_token() }}'
                    },
                    success: function(hasil) {
                        if (hasil != "") {
                            var res = JSON.parse(hasil);
                            if (res != undefined && res.url != "" && res.url != null) {
                                location.replace(res.url);
                            } else {
                                location.reload();
                            }
                        } else {
                            location.reload();
                        }
                    },
                    error: function(a, b, c) {
                        if (a.status == 403) {
                            ShowError("Anda tidak punya akses");
                        } else if (a.status == 404) {
                            ShowError("Data tidak ditemukan");
                        } else {
                            ShowError(b);
                        }
                    }
                });
            }
        });
    }
</script>
