<div class="modal fade" id="main_modal">
    <div id="modal-custom" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="display: none">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="judul_modal">Default Modal</h4>
            </div>
            <div class="modal-body" id="isi_modal">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script>
    function OpenModal(url, xl) {

        if (xl == undefined) {
            $("#modal-custom").removeClass("modal-xl");
            $("#modal-custom").addClass("modal-lg");
        } else {
            $("#modal-custom").removeClass("modal-lg");
            $("#modal-custom").addClass("modal-xl");
        }

        ShowLoading();
        $.ajax({
            url: url,
            success: function(res) {
                swal.close();
                $("#isi_modal").html(res);
                $("#main_modal").modal("show");
            },
            error: function(a, b, c) {
                swal.close();
                if (a.status == 403) {
                    ShowError("Anda tidak punya akses");
                } else if (a.status == 404) {
                    ShowError("Data tidak ditemukan.");
                } else {
                    ShowError(b);
                }
            }
        });
    }

    function OpenImage(url) {
        OpenModal('@Url.Action("OpenImage","Home", new {area=""})?i=' + url, 1);
    }
</script>