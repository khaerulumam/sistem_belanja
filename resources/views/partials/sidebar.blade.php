<nav class="mt-2 text-sm">
    <ul class="nav nav-pills nav-sidebar nav-child-indent flex-column" data-widget="treeview" role="menu"
        data-accordion="false" id="sidebar">
        <!-- Add icons to the links using the .nav-icon class
with font-awesome or any other icon font library -->
        <li class="nav-header pt-4">Menu Utama</li>
        <li class="nav-item">
            <a href="{{ route('admin', [], false) }}" class="nav-link">
                <i class="nav-icon fa fa-th-large"></i>
                <p>
                    Home
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('editor-belanja', [], false) }}" class="nav-link">
                <i class="nav-icon fas fa-shopping-basket"></i>
                <p>
                    Belanja
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('editor-barang', [], false) }}" class="nav-link">
                <i class="nav-icon fas fa-boxes"></i>
                <p>
                    Barang
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('editor-jenis-barang', [], false) }}" class="nav-link">
                <i class="nav-icon fas fa-box"></i>
                <p>
                    Jenis Barang
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('editor-user', [], false) }}" class="nav-link">
                <i class="nav-icon fas fa-user-cog"></i>
                <p>
                    Manajemen Pengguna
                </p>
            </a>
        </li>
    </ul>
</nav>

<script>
    var area = ['admin'];
    var exception = [];
    var currentUrl = '{{ Request::path() }}';
    var split = currentUrl.split("/");

    var currentController = split[0];
    if (area.includes(currentController)) {
        currentController = split[1];
        if (currentController == undefined) {
            currentController = split[0];
        }
    }

    $(document).ready(function() {
        $("#sidebar a.nav-link").each(function() {
            let link = $(this).attr("href");
            if (link == undefined) return;

            var list = $(this).data('url');
            if (list == undefined) {
                list = [];
            }
            list.push(link);

            var obj = $(this);

            $(list).each(function(index, item) {
                let splitLink = item.split("/");
                let controller = splitLink[1];
                if (area.includes(controller)) {
                    controller = splitLink[2];
                    if (controller == undefined) {
                        controller = splitLink[1];
                    }
                }

                if (controller === currentController) {
                    obj.addClass("active");
                    return;
                }
            });
        });
        $(".nav-sidebar .nav-treeview").each(function() {
            let panjang = $(this).find("a.nav-link.active").length;
            if (panjang) {
                $(this).parent().addClass("menu-open");
                $(this).prev().addClass("active");
            }
        });
    });
</script>
