<script src="/lib/chartjs/chart.min.js"></script>
<script>
    const warna = {
        'merah': 'rgba(255,0,0,1)',
        'hijau': 'rgba(0,255,0,1)',
        'biru': 'rgba(0,0,255,1)',
        'kuning': 'rgba(255,255,0,1)',
    };


    function GenerateChart(dataChart, idElement) {
        idElement = idElement || 'myChart';
        let ctx = document.getElementById(idElement);

        dataChart = dataChart.replaceAll('&quot;', '"');
        let chart = JSON.parse(dataChart);

        let data = {
            labels: chart.label,
            datasets: [{
                label: 'Nilai',
                data: chart.aktual,
                backgroundColor: 'rgba(0,0,255,1)',
                borderColor: 'rgba(0,0,255,1)',
                yAxisID: 'y',
            }]
        }

        let config = {
            type: 'line',
            data: data,
            options: {
                responsive: true,
                interaction: {
                    mode: 'index',
                    intersect: false,
                },
                stacked: false,
                plugins: {
                    title: {
                        display: false,
                        text: 'Tingkat Kemiskinan'
                    }
                },
                scales: {
                    y: {
                        type: 'linear',
                        display: true,
                        position: 'left',
                    },
                    // y1: {
                    //     type: 'linear',
                    //     display: true,
                    //     position: 'right',

                    //     // grid line settings
                    //     grid: {
                    //         drawOnChartArea: false, // only want the grid lines for one axis to show up
                    //     },
                    // },
                }
            },
        };

        let mychart = new Chart(ctx, config);
    }

    function GenerateChartProyeksi(dataChart, idElement) {
        idElement = idElement || 'myChart';
        let ctx = document.getElementById(idElement);

        dataChart = dataChart.replaceAll('&quot;', '"');
        let chart = JSON.parse(dataChart);

        let data = {
            labels: chart.label,
            datasets: [{
                label: 'Nilai Aktual',
                data: chart.aktual,
                backgroundColor: warna.biru,
                borderColor: warna.biru,
                yAxisID: 'y',
            }, {
                label: 'Nilai Proyeksi Pesimis',
                data: chart.pesimis,
                backgroundColor: warna.hijau,
                borderColor: warna.hijau,
                yAxisID: 'y',
            }, {
                label: 'Nilai Proyeksi Moderat',
                data: chart.moderat,
                backgroundColor: warna.kuning,
                borderColor: warna.kuning,
                yAxisID: 'y',
            }, {
                label: 'Nilai Proyeksi Optimis',
                data: chart.optimis,
                backgroundColor: warna.merah,
                borderColor: warna.merah,
                yAxisID: 'y',
            }, ]
        };

        let config = {
            type: 'line',
            data: data,
            options: {
                responsive: true,
                interaction: {
                    mode: 'index',
                    intersect: false,
                },
                stacked: false,
                plugins: {
                    title: {
                        display: false,
                        text: 'Tingkat Kemiskinan'
                    }
                },
                scales: {
                    y: {
                        type: 'linear',
                        display: true,
                        position: 'left',
                    },
                    // y1: {
                    //     type: 'linear',
                    //     display: true,
                    //     position: 'right',

                    //     // grid line settings
                    //     grid: {
                    //         drawOnChartArea: false, // only want the grid lines for one axis to show up
                    //     },
                    // },
                }
            },
        };

        let mychart = new Chart(ctx, config);
    }
</script>
