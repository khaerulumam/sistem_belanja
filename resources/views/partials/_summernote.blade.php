<link rel="stylesheet" href="/lib/template/plugins/summernote/summernote-bs4.min.css">
<script src="/lib/template/plugins/summernote/summernote-bs4.min.js"></script>

<script>
    function SetSummernote(id) {
        id = id || "summernote";
        id = "#" + id;

        $(id).summernote({
            height: 500
        });
    }
</script>
