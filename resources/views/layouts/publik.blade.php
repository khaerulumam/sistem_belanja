<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        @yield('title') | {{ config('app.name') }}
    </title>

    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet">

    <link rel="icon" href="/img/logo.png">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="/lib/template/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/lib/template/dist/css/adminlte.css">
    <link rel="stylesheet" href="/css/site.css">

    <link rel="stylesheet" href="/lib/template/plugins/ekko-lightbox/ekko-lightbox.css">
    @yield('styles')
</head>

<body class="hold-transition layout-top-nav">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
            <div class="container">
                <a href="/" class="navbar-brand">
                    <img src="/img/logo.png" alt="Dashboard Kota Bogor Logo" class="brand-image elevation-3"
                        style="opacity: .8">
                    <span class="brand-text font-weight-light">{{ config('app.name') }}
                    </span>
                </a>
                <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                    aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon">
                    </span>
                </button>
                <div class="collapse navbar-collapse order-3" id="navbarCollapse">
                    <ul class="navbar-nav navbar-no-expand ml-auto">
                        @include('partials._navbar')
                        @auth
                            <li class="nav-item">
                                <a class="nav-link"
                                    onclick="KonfirmasiSubmit('logout-form','Anda akan keluar dari sistem.')" role="button">
                                    <i class="fas fa-sign-out-alt">
                                    </i>
                                </a>
                            </li>
                            <form id="logout-form" action="{{ route('logout', [], false) }}" method="POST"
                                style="display: none;">
                                @csrf
                            </form>
                        @endauth
                    </ul>
                </div>


            </div>
        </nav>
        <!-- /.navbar -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="jumbotron"
                style="background-image: url(/img/bg-banner.png);background-repeat: no-repeat;min-width: 100%;background-size: cover;border-bottom-right-radius: 100px;">
                <div class="container">
                    <h1 class="text-white display-5">
                        @hasSection('header')
                            @yield('header')
                        @else
                            @yield('title')
                        @endif
                    </h1>
                </div>
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <div class="content pt-4 pb-5">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            @yield('content')
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- Main Footer -->
    <footer class="main-footer text-center">
        <!-- To the right -->
        <div class=" d-sm-inline">
            <strong>Copyright &copy; 2021
                <a href="/">Sistem Informasi Makro Ekonomi Kota Bogor
                </a>.
            </strong> All rights
            reserved.
        </div>
        <!-- Default to the left -->
    </footer>
    </div>
    <!-- ./wrapper -->
    <!-- REQUIRED SCRIPTS -->
    <style>
        .jumbotron {
            padding: 5rem 2rem;
            border-radius: 0;
        }

        .navbar-brand {
            font-size: 1rem;
        }

        .nav-pills-custom .nav-link {
            color: #aaa;
            background: #fff;
            position: relative;
        }

        .nav-pills-custom .nav-link.active {
            color: #323A46;
            background: #fff;
        }

        .tab-content>.active {
            min-height: 659px;
        }

        .info-box .info-box-icon {
            width: 48px;
            font-size: 1.5rem;
        }

        .info-box .info-box-content {
            line-height: 1.4;
        }

        .shadow {
            box-shadow: 18px 4px 35px rgba(0, 0, 0, 0.04) !important;
        }

    </style>
    <!-- jQuery -->
    <script src="/lib/template/plugins/jquery/jquery.min.js">
    </script>
    <!-- Bootstrap 4 -->
    <script src="/lib/template/plugins/bootstrap/js/bootstrap.bundle.min.js">
    </script>
    <!-- AdminLTE App -->
    <script src="/lib/template/dist/js/adminlte.min.js">
    </script>
    <script src="/lib/template/plugins/sweetalert2/sweetalert2.all.min.js">
    </script>
    <script src="/lib/template/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
    @include('partials._jquery_form')
    @include('partials._modal')
    <script src="/js/site.js">
    </script>
    <!-- AdminLTE for demo purposes -->
    {{-- <script src="/lib/template/dist/js/demo.js">
    </script> --}}
    <script>
        $(function() {
            $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox({
                    alwaysShowClose: true
                });
            });

            // $('.filter-container').filterizr({
            //     gutterPixels: 3
            // });
            $('.btn[data-filter]').on('click', function() {
                $('.btn[data-filter]').removeClass('active');
                $(this).addClass('active');
            });
        })
    </script>
    @yield('scripts')
</body>

</html>
