@php
$isEdit = $data->id > 0;
@endphp

<h3>
    <b>
        {{ $isEdit ? 'Ubah' : 'Tambah' }} Belanja
    </b>
</h3>
<hr>
<form
    action="{{ $isEdit? route('editor-belanja-ep', ['id' => $data->id], false): route('editor-belanja-cp', [], false) }}"
    method="POST" id="main_form">
    @if ($isEdit)
        <input type="hidden" name="id" value="{{ $data->id }}">
    @endif
    <div class="form-group">
        <label class="harus">Barang</label>
        <select class="form-control chosen" name="id_barang">
            <option value="">-- Pilih --</option>
            @foreach ($jenis as $x)
                <option value="{{ $x->id }}" {{ $x->id == $data->id_barang ? "selected" : "" }}>{{ $x->jenisBarang->nama }} ({{ $x->merk }})</option>
            @endforeach
        </select>
        <label id="id_barang-error" class="error" for="id_barang" style="display: none">This field is required.</label>
    </div>
    <div class="form-group">
        <label class="harus">Jumlah Barang</label>
        <input type="number" class="form-control" name="jumlah_barang" value="{{ $data->jumlah_barang }}">
    </div>
    <div class="form-group">
        <label class="harus">Jumlah Harga</label>
        <input type="text" class="form-control uang" name="jumlah_harga" value="{{ $data->jumlah_harga }}">
    </div>
    <div class="form-group">
        <label class="harus">Periode</label>
        <input type="text" class="form-control periode" name="periode" value="{{ $data->periode }}">
    </div>
    <div class="form-group">
        <label class="">Pembeli</label>
        <select class="form-control chosen" name="id_user_pembeli">
            <option value="">-- Pilih --</option>
            @foreach ($user as $x)
                <option value="{{ $x->id }}" {{ $x->id == $data->id_user_pembeli ? "selected" : "" }}>{{ $x->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group text-right">
        <button class="btn btn-primary" type="submit">Simpan</button>
    </div>
</form>
<script>
    SetValidation();
    SetChosen();
    SetPeriode();
    SetMaskMoney();
</script>
