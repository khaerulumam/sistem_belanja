@extends('layouts.web')

@section('title')
    Belanja
@endsection

@section('content')
    <div class="card card-primary card-outline">
        <div class="card-body">
            <div class="w-100 text-right mb-3">
                <a class="btn btn-primary" onclick="OpenModal('{{ route('editor-belanja-c', [], false) }}')">Tambah</a>
            </div>
            <table class="table" id="main_table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Periode</th>
                        <th>Jenis Barang</th>
                        <th>Merk</th>
                        <th>Jumlah Barang</th>
                        <th>Jumlah Harga</th>
                        <th>Pembeli</th>
                        <th class="nosort">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($data as $item)
                        <tr>
                            <td>{{ $no }}</td>
                            <td>{{ date('Y-m', strtotime($item->periode)) }}</td>
                            <td>{{ $item->barang->jenisBarang->nama }}</td>
                            <td>{{ $item->barang->merk }}</td>
                            <td>{{ $item->jumlah_barang }}</td>
                            <td>{{ number_format($item->jumlah_harga) }}</td>
                            <td>{{ $item->user != null ? $item->user->name : '-' }}</td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-warning"
                                        onclick="OpenModal('{{ route('editor-belanja-e', [$item->id], false) }}')"><i
                                            class="fa fa-edit"></i></a>
                                    <a class="btn btn-danger"
                                        onclick="Delete('{{ route('editor-belanja-d', [$item->id], false) }}')"><i
                                            class="fa fa-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                        @php
                            $no++;
                        @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    @include('partials._mask-money')
    @include('partials._datepicker')
    @include('partials._select2')
    @include('partials._datatables')
@endsection
