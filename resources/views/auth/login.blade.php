<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login | {{ config('app.name') }}</title>

    <link rel="icon" href="/img/dota2.png">
    <!-- Google Font: Source Sans Pro -->
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600&display=swap"
        rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/lib/template/plugins/fontawesome-free/css/all.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="/lib/template/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/lib/template/dist/css/adminlte.min.css">
    <link rel="stylesheet" href="/css/site.css">
</head>

<body class="hold-transition login-page">
    <div class="website-logo">
        <a href="/">
            <div class="logo">
                {{-- <img class="logo-size" src="/img/dota2.png" alt=""> --}}
            </div>
        </a>
    </div>
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="card card-outline">
            <div class="card-header nohr">
                <a href="#" class="h3"><b>Login</b></a>
                <p class="login-box-msg">Pastikan anda telah memiliki akun</p>
            </div>
            <div class="card-body">


                <form method="POST" action="{{ route('login', [], false) }}" id="main_form">
                    @csrf
                    <div class=" mb-3">
                        <input type="text" class="form-control" placeholder="Username" name="username" required>

                        <label id="username-error" class="error w-100" for="username" style="display: none">This
                            field is required.</label>
                    </div>
                    <div class=" mb-3">
                        <input type="password" class="form-control" placeholder="Password" name="password" required>

                        <label id="password-error" class="error w-100" for="password" style="display: none">This
                            field is required.</label>
                    </div>
                    <div class="row">
                        <div class="col-12 ">
                            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                        </div>
                    </div>
                    
                </form>

            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="/lib/template/plugins/jquery/jquery.min.js"></script>
    <script src="/lib/template/plugins/jquery-validation/jquery.validate.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="/lib/template/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/lib/template/dist/js/adminlte.min.js"></script>
    <script src="/lib/template/plugins/sweetalert2/sweetalert2.all.min.js"></script>
    @include('partials._jquery_form')
    <script src="/js/site.js"></script>

    <script>
        $("#main_form").validate();
        SetJqueryForm();
    </script>
</body>

</html>
