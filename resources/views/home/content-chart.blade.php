<canvas id="myChart-{{ $kode }}" width="600" height="400"></canvas>

<script>
    {
        if ('{{ $kode }}' == 'pdrb') {
            GenerateChart('{{ $chart }}', 'myChart-{{ $kode }}');
        } else if ('{{ $kode }}' == 'lpe') {
            GenerateChartProyeksi('{{ $chart }}', 'myChart-{{ $kode }}');
        }
    }
</script>
