<h4 class="mb-4">{{ $nama }}
</h4>
<canvas id="myChart{{ $kode }}" width="600" height="400"></canvas>
<div class="pt-4 pb-3">
    <a href="{{ $link }}" class="btn btn-outline-primary float-right">Detail Data
    </a>
    <a href="{{ $export }}" class="btn btn-default float-right mr-2">Export</a>
</div>

<script>
    GenerateChartProyeksi('{{ $chart }}', 'myChart{{ $kode }}');
</script>
