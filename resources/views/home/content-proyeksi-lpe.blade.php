<h4 class="mb-4">{{ $nama }}
</h4>
<select id="sektor-{{ $kode }}" class="form-control chosen mb-5">
    @foreach ($sektor as $item)
        <option value="{{ $item->id }}">{{ $item->nama }}</option>
    @endforeach
</select>
<div id="tempat-chart-{{ $kode }}">
    <div class="pt-4 pb-3 text-center">
        <i class="fa fa-spinner fa-spin"></i>
    </div>
</div>
<div class="pt-4 pb-3">
    <a href="{{ $link }}" class="btn btn-outline-primary float-right">Detail Data
    </a>
    @switch($kode)
        @case('pdrb')
            <a onclick="exportPDRB()" class="btn btn-default float-right mr-2">Export</a>
        @break
        @case('lpe')
            <a onclick="exportLPE()" class="btn btn-default float-right mr-2">Export</a>
        @break
    @endswitch
</div>

<script>
    SetChosen();
    getChart('{{ $kode }}', $("#sektor-{{ $kode }}").val());
    $("#sektor-{{ $kode }}").change(function() {
        getChart('{{ $kode }}', $("#sektor-{{ $kode }}").val());
    });
</script>
