@extends('layouts.publik')
@section('title')
    Home
@endsection
@section('header')
    Sistem Informasi
    <br />
    Ekonomi Makro Kota Bogor
    <p class="lead text-white pb-4">
        Data pendapatan nasional adalah salah satu indikator makro yang dapat menunjukkan kondisi perekonomian nasional
        setiap tahun. Manfaat yang dapat diperoleh dari data ini antara lain adalah
    <p class="lead">
        <a class="btn btn-warning" href="{{ route('profil', [], false) }}" role="button">Tentang Kami
        </a>
    </p>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-4">
            <!-- Tabs nav -->
            <div class="nav flex-column nav-pills nav-pills-custom" id="v-pills-tab" role="tablist"
                aria-orientation="vertical">
                <a class="info-box nav-link mb-3 p-3 shadow active" data-toggle="pill" href="#v-pills-lpe" role="tab"
                    aria-selected="true">
                    <span class="info-box-icon bg-indigo">
                        <img src="{{ url('img/stats-chart.svg') }}" alt="" class="" style="max-width:50%;">
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-number">
                            Laju Pertumbuhan Ekonomi (LPE)
                        </span>
                    </div>
                </a>
                <a class="info-box nav-link mb-3 p-3 shadow" data-toggle="pill" href="#v-pills-pdrb" role="tab"
                    aria-selected="false">
                    <span class="info-box-icon bg-teal">
                        <i class="fa fa-chart-bar">
                        </i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-number">
                            Produk Domestik Regional Bruto (PDRB)
                        </span>
                    </div>
                </a>
                <a class="info-box nav-link mb-3 p-3 shadow" data-toggle="pill" href="#v-pills-inflasi" role="tab"
                    aria-selected="false">
                    <span class="info-box-icon bg-red">
                        <i class="fa fa-chart-bar">
                        </i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-number">
                            Inflasi
                        </span>
                    </div>
                </a>
                <a class="info-box nav-link mb-3 p-3 shadow" data-toggle="pill" href="#v-pills-ipm" role="tab"
                    aria-selected="false">
                    <span class="info-box-icon bg-blue">
                        <i class="fa fa-chart-bar">
                        </i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-number">
                            Indeks Pembangunan Manusia
                        </span>
                    </div>
                </a>
                <a class="info-box nav-link mb-3 p-3 shadow" data-toggle="pill" href="#v-pills-kemiskinan" role="tab"
                    aria-selected="false">
                    <span class="info-box-icon bg-green">
                        <i class="fa fa-chart-bar">
                        </i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-number">
                            Tingkat Kemiskinan
                        </span>
                    </div>
                </a>
                <a class="info-box nav-link mb-3 p-3 shadow" data-toggle="pill" href="#v-pills-gini" role="tab"
                    aria-selected="false">
                    <span class="info-box-icon bg-warning">
                        <i class="fa fa-chart-bar">
                        </i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-number">
                            Indeks Gini
                        </span>
                    </div>
                </a>
                <a class="info-box nav-link mb-3 p-3 shadow" data-toggle="pill" href="#v-pills-TPT" role="tab"
                    aria-selected="false">
                    <span class="info-box-icon bg-blue">
                        <i class="fa fa-chart-bar">
                        </i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-number">
                            Tingkat Pengangguran Terbuka (TPT)
                        </span>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-8">
            <!-- Tabs content -->
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade shadow rounded bg-white show active p-5" id="v-pills-lpe" role="tabpanel">
                    <div class="pt-4 pb-3 text-center">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                </div>
                <div class="tab-pane fade shadow rounded bg-white p-5" id="v-pills-pdrb" role="tabpanel">
                    <div class="pt-4 pb-3 text-center">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                </div>
                <div class="tab-pane fade shadow rounded bg-white p-5" id="v-pills-inflasi" role="tabpanel">
                    <div class="pt-4 pb-3 text-center">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                </div>
                <div class="tab-pane fade shadow rounded bg-white p-5" id="v-pills-ipm" role="tabpanel">
                    <div class="pt-4 pb-3 text-center">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                </div>
                <div class="tab-pane fade shadow rounded bg-white p-5" id="v-pills-kemiskinan" role="tabpanel">
                    <div class="pt-4 pb-3 text-center">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                </div>
                <div class="tab-pane fade shadow rounded bg-white p-5" id="v-pills-gini" role="tabpanel">
                    <div class="pt-4 pb-3 text-center">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                </div>
                <div class="tab-pane fade shadow rounded bg-white p-5" id="v-pills-TPT" role="tabpanel">
                    <div class="pt-4 pb-3 text-center">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('partials._select2')
    @include('partials._chart')
    <script>
        getContent('lpe');
        getContent('pdrb');
        getContent('inflasi');
        getContent('ipm');
        getContent('kemiskinan');
        getContent('gini');
        getContent('TPT');

        function getContent(id) {
            $.get("{{ route('home-content', [], false) }}/" + id, function(res) {
                $("#v-pills-" + id).html(res);
            })
        }

        function getChart(kode, sektor) {
            let url = "{{ route('chart-content', ['kode' => ':kode', 'sektor' => ':sektor'], false) }}";
            url = url.replace(':kode', kode).replace(':sektor', sektor);
            $.get(url, function(res) {
                $("#tempat-chart-" + kode).html(res);
            })
        }

        function exportPDRB() {
            let sektor = $("#sektor-pdrb").val();
            let url = "{{ route('export.pdrb', ['sektor' => ':sektor'], false) }}";
            url = url.replace(':sektor', sektor);
            location.href = url;
        }

        function exportLPE() {
            let sektor = $("#sektor-lpe").val();
            let url = "{{ route('export.lpe', ['sektor' => ':sektor'], false) }}";
            url = url.replace(':sektor', sektor);
            location.href = url;
        }
    </script>
@endsection
