function SetValidation(idform) {
    idform = idform || "main_form";
    SetRequired();
    $("#" + idform).validate();
    SetJqueryForm(idform);
}

function SetRequired() {
    $(".harus").each(function () {
        var input = $(this).parent().find(":input").first();
        input.attr('required', true);
    })
}

function ShowError(message) {
    Swal.fire(
        "Gagal!",
        message,
        "error"
    );
}

function ShowSuccess(message) {
    Swal.fire(
        "Berhasil!",
        message,
        "success"
    );
}

function ShowInfo(message) {
    Swal.fire(
        "Info!",
        message,
        "info"
    );
}

function ShowDevelopmentInfo() {
    ShowInfo("Maaf, fitur ini sedang dalam masa pengembangan.");
}

function ShowLoading() {
    Swal.fire({
        title: 'Mohon tunggu...!',
        //html: 'data uploading',// add html attribute if you want or remove
        allowOutsideClick: false,
        willOpen: () => {
            Swal.showLoading();
        }
    });
}

function KonfirmasiSubmit(idForm, message) {
    idForm = idForm || "main_form";
    Konfirmasi(message,
        () => {
            $("#" + idForm).submit();
        });
}

function Konfirmasi(message, callback) {
    message = message || "Anda akan mengirim form ini!";
    Swal.fire({
        title: 'Apakah Anda yakin?',
        text: message,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Batal',
        confirmButtonText: 'Ya'
    }).then((result) => {
        if (result.value) {
            callback();
        }
    });
}