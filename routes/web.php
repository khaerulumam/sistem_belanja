<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\BelanjaController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\JenisBarangController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('role:admin')->group(function () {
    Route::prefix('admin')->group(function () {
        Route::get('/', [DashboardController::class, 'index'])->name('admin');

        Route::name('editor-')->group(function () {
            Route::get('/user', [UserController::class, 'index'])->name('user');
            Route::get('/user/create', [UserController::class, 'create'])->name('user_c');
            Route::post('/user/create', [UserController::class, 'store'])->name('user_cp');
            Route::get('/user/edit/{id}', [UserController::class, 'edit'])->name('user_e');
            Route::post('/user/edit/{id}', [UserController::class, 'update'])->name('user_ep');
            Route::post('/user/delete/{id}', [UserController::class, 'destroy'])->name('user_d');
            
            Route::get('/jenis-barang', [JenisBarangController::class, 'index'])->name('jenis-barang');
            Route::get('/jenis-barang/create', [JenisBarangController::class, 'create'])->name('jenis-barang-c');
            Route::post('/jenis-barang/create', [JenisBarangController::class, 'store'])->name('jenis-barang-cp');
            Route::get('/jenis-barang/edit/{id}', [JenisBarangController::class, 'edit'])->name('jenis-barang-e');
            Route::post('/jenis-barang/edit/{id}', [JenisBarangController::class, 'update'])->name('jenis-barang-ep');
            Route::post('/jenis-barang/delete/{id}', [JenisBarangController::class, 'destroy'])->name('jenis-barang-d');
            
            Route::get('/barang', [BarangController::class, 'index'])->name('barang');
            Route::get('/barang/create', [BarangController::class, 'create'])->name('barang-c');
            Route::post('/barang/create', [BarangController::class, 'store'])->name('barang-cp');
            Route::get('/barang/edit/{id}', [BarangController::class, 'edit'])->name('barang-e');
            Route::post('/barang/edit/{id}', [BarangController::class, 'update'])->name('barang-ep');
            Route::post('/barang/delete/{id}', [BarangController::class, 'destroy'])->name('barang-d');
            
            Route::get('/belanja', [BelanjaController::class, 'index'])->name('belanja');
            Route::get('/belanja/create', [BelanjaController::class, 'create'])->name('belanja-c');
            Route::post('/belanja/create', [BelanjaController::class, 'store'])->name('belanja-cp');
            Route::get('/belanja/edit/{id}', [BelanjaController::class, 'edit'])->name('belanja-e');
            Route::post('/belanja/edit/{id}', [BelanjaController::class, 'update'])->name('belanja-ep');
            Route::post('/belanja/delete/{id}', [BelanjaController::class, 'destroy'])->name('belanja-d');
        });
    });
});

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::post("/login", [LoginController::class, 'login'])->middleware("throttle:100,2");
